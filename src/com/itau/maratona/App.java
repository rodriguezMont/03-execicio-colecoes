package com.itau.maratona;

import java.util.List;

public class App {

	public static void main(String[] args) {
		
		// Lendo o Arquivo
		Arquivo arquivo = new Arquivo("alunos.csv");
		
		// Montando lista de arquivos a partir do arquivo
		List<Aluno> alunos = arquivo.ler();		
		
		// Sorteando os alunos e montando as equipes
		Equipe[] equipes = CriadorEquipes.construir(alunos);
		
		// Imprimindo a lista de equipes e alunos
		Impressora.imprimirLista(equipes);
		
	}
	
}

package com.itau.maratona;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CriadorEquipes {

	public static Equipe[] construir(List<Aluno> alunos) {
		
		alunos = sortearAlunos(alunos);
		
		// CONTINUAR DAQUI!
		Equipe[] equipes = new Equipe[alunos.size() / 3];
		
		for(int i = 0;  i < equipes.length; i++) {
			
			Equipe equipe = new Equipe();
			equipe.setId(i + 1);
			
			Aluno[] alunosEquipe = new Aluno[3];
			for(int alunoPos = 0; alunoPos < 3; alunoPos++) {
				alunosEquipe[alunoPos] = alunos.get(alunoPos);
			}
			alunos.subList(0, 2).clear();		
			equipe.setAlunos(alunosEquipe);
			
			equipes[i] = equipe;
		}
		
		return equipes;
	}

	private static List<Aluno> sortearAlunos(List<Aluno> alunos) {

//		Double valor = Math.ceil(Math.random() * 6);
//		int valorInteiro = valor.intValue();
		
		int count = alunos.size();

		List<Aluno> alunosSorteados = new ArrayList<>();

		do {

			Double posicaoLista = Math.ceil(Math.random() * alunos.size());
			int posicaoFinal = posicaoLista.intValue();
			
			alunosSorteados.add(alunos.get(posicaoFinal - 1));
			alunos.remove(posicaoFinal - 1);
			
		} while (alunos.size() != 0);

		return alunosSorteados;
	}

}

package com.itau.maratona;

public class Impressora {

	public static void imprimirLista(Equipe[] equipes) {
		
		for(Equipe equipe : equipes) {
			System.out.println("Equipe Número " + equipe.getId());
			System.out.println("Membros da Equipe:");
			for(Aluno aluno : equipe.getAlunos()) {
				System.out.println(" - " + aluno.nome);
			}
			System.out.println();
		}
		
	}
	
}
